# Veřejná databáze narutolarpu

> Detailnější popis je rozpracovaný na https://narutolarp.gitlab.io/database-public/info/

Jádrem databáze jsou XML soubory obsahující samotná data
* `public/data/strom-*.xml` - všechny technik ve hře, každý strom má vlastní soubor
* `public/data/definice.xml` - zásahy, hesla, etc.
* `public/data/specialni-schopnosti.xml` - všechny speciální schopnosti

Techniky je možné zobrazit na
* https://narutolarp.gitlab.io/techniky/skilltree.html
* https://narutolarp.gitlab.io/techniky/list.html
* https://narutolarp.gitlab.io/techniky/table.html

Zobrazování technik mají nastarosti jednotlivé Elm aplikace v `skilltree/src/App/`


## Jak pracovat lokálně

https://wiki.python.org/moin/BeginnersGuide/Download 3.11 nebo novější

https://guide.elm-lang.org/install/elm

### XML

`public/data/` - pro editaci XML stačí jakýkoli editor, který umí XSD schema.

XSD schema máme v `public/data/schemas/` - na ty většinou není potřeba sahat.

Některá schémata (`*-generated.xsd`) jsou generovaná skriptem `utils/generate_enums.py`,
ten je potřeba spouštět jen pokud se přidalo/odebralo nějaké id (např nová/smazaná technika).
(Tedy po naklonování repozitáře není potřeba spouštět.)  
Příkaz se spouští z rootu projektu, tedy příkazem:  
`python3 utils/generate_enums.py` nebo `python utils/generate_enums.py`

### Frontend (zobrazování technik)

frontend používá https://guide.elm-lang.org

Pro vývoj je potřeba být uvnitř složky skilltree/ ().
Projekt je třeba zkompilovat pomocí skriptu src/build.sh.
```sh
cd skilltree
sh build.sh
```
Což ve složce `public/` vygeneruje soubory `bundle-*.js` (kde `*` je `Skilltree`, `List` nebo `Table`).  
Poté spustit libovolný http server (pro statický obsah) ve složce `public/`
```sh
cd public
python3 -m http.server
```
A v prohlížečí otevřít servírovanou adresu (a jeden z html souborů)
