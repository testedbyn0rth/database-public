from __future__ import annotations
from collections import defaultdict
from contextlib import contextmanager
from itertools import chain
from pathlib import Path
import re
from xml.etree import ElementTree as ET
import xml.dom
import xml.dom.minidom as dom
from typing import Generator, Iterable, NamedTuple, Iterator


XML_NS = {"xsi": "http://www.w3.org/2001/XMLSchema-instance"}
XSD_NS = {"xs": "http://www.w3.org/2001/XMLSchema"}

CHAR = 'ěščřžýáíéůúň'
CHAR += CHAR.upper()
CHAR += 'a-zA-Z'

VALID_ID_PATTER = f'[{CHAR}][{CHAR}0-9_-]+[{CHAR}0-9]'
VALID_ID = re.compile(VALID_ID_PATTER)

DIRNAME_SCHEMAS = 'schemas'
DIRNAME_UTILS = 'utils'


##########################
# SHARED DATA STRUCTURES #

class IdDef(NamedTuple):
    id: str
    file: Path
    enum_name: str

    def location(self) -> str:
        return f"{self.file}:{self.enum_name}"


class EnumData(NamedTuple):
    name: str
    ids: list[IdDef]

    def add(self, other: EnumData) -> EnumData:
        assert self.name == other.name
        return EnumData(self.name, self.ids + other.ids)


#######################
# GENERATE OUTPUT XSD #


def _create_enum_definition(enum: EnumData, document: dom.Document) -> dom.Element:
    simple_type: dom.Element = document.createElementNS(
        XSD_NS["xs"], "xs:simpleType")
    simple_type.setAttribute("name", enum.name)
    simple_type.setAttribute("final", "restriction")

    restriction: dom.Element = document.createElementNS(
        XSD_NS["xs"], "xs:restriction")
    simple_type.appendChild(restriction)
    restriction.setAttribute("base", "xs:string")

    for id_def in sorted(enum.ids, key=lambda x: x.id):
        enumeration: dom.Element = document.createElementNS(
            XSD_NS["xs"], "xs:enumeration"
        )
        restriction.appendChild(enumeration)
        enumeration.setAttribute("value", id_def.id)

    return simple_type


def _create_enum_reference_definition(
    enum: EnumData, document: dom.Document
) -> dom.Element:
    complex_type: dom.Element = document.createElementNS(
        XSD_NS["xs"], "xs:complexType")
    complex_type.setAttribute("name", f"reference-{enum.name}")

    attribute: dom.Element = document.createElementNS(
        XSD_NS["xs"], "xs:attribute")
    complex_type.appendChild(attribute)
    attribute.setAttribute("name", "ref")
    attribute.setAttribute("type", enum.name)
    attribute.setAttribute("use", "required")

    return complex_type


def _create_document(data: Iterable[EnumData], targetNamespace: str) -> dom.Document:
    DOM = dom.getDOMImplementation()
    assert DOM
    document = DOM.createDocument(XSD_NS["xs"], "xs:schema", None)
    document_root: dom.Element = document.documentElement
    document_root.setAttributeNS(
        xml.dom.XMLNS_NAMESPACE, "xmlns:xs", "http://www.w3.org/2001/XMLSchema"
    )
    document_root.setAttribute("targetNamespace", targetNamespace)
    document_root.setAttribute("xmlns", targetNamespace)

    comment = document.createComment(
        " Nesahat! Soubor je generovan skriptem generate_enums.py "
    )
    document_root.appendChild(comment)

    for enum in data:
        document_root.appendChild(_create_enum_definition(enum, document))
        document_root.appendChild(
            _create_enum_reference_definition(enum, document))

    return document


def _write_to_file(document: dom.Document, path: Path):
    xml_string = document.toprettyxml(encoding="UTF-8")
    with open(path, "wb") as document_file:
        document_file.write(xml_string)


def generate_single_document(
    data: Iterable[EnumData], target_namespace: str, target_path: Path
) -> None:
    new_document = _create_document(data, target_namespace)
    _write_to_file(new_document, target_path)


#######################
# PARSE INPUT XML+XSD #


def _open_xml_with_schema(
    xml_file: Path,
) -> tuple[ET.ElementTree, ET.ElementTree, Path]:
    xml_document = ET.parse(xml_file)
    schema_relative_path: str = xml_document.getroot().attrib[
        str(ET.QName(XML_NS["xsi"], "noNamespaceSchemaLocation"))
    ]

    schema_path = xml_file.parent / schema_relative_path
    schema_document = ET.parse(schema_path)

    return xml_document, schema_document, schema_path


_find_ids__valid_field = re.compile(r"@[a-z-]+")


def _find_ids(key_el: ET.Element, xml_document: ET.ElementTree) -> Iterable[str]:
    selector_el = key_el.find(".//xs:selector", XSD_NS)
    assert selector_el is not None
    selector = selector_el.attrib["xpath"]
    field_el = key_el.find(".//xs:field", XSD_NS)
    assert field_el is not None
    field = field_el.attrib["xpath"]
    assert _find_ids__valid_field.match(field)
    for instance in xml_document.findall(selector, XML_NS):
        yield instance.attrib[field.lstrip("@")]


def _find_enum_data(
    xml_file: Path,
    xml_document: ET.ElementTree,
    schema_document: ET.ElementTree,
) -> Iterable[EnumData]:
    for key_el in schema_document.findall(".//xs:key", XSD_NS):
        name = key_el.attrib["name"]
        ids = list(_find_ids(key_el, xml_document))

        yield EnumData(name, [IdDef(id, xml_file, name) for id in ids])


def parse_single_xml(xml_file: Path) -> tuple[Path, list[EnumData]]:
    xml_document, schema_document, schema_path = (
        _open_xml_with_schema(xml_file))
    assert schema_path.parent.samefile(xml_file.parent / DIRNAME_SCHEMAS)
    data = list(_find_enum_data(xml_file, xml_document, schema_document))
    return schema_path, data


##############
# MAIN LOGIC #


class FormatError(NamedTuple):
    id_def: IdDef

    def __str__(self) -> str:
        return "\n".join([
            f"Id '{self.id_def.id}' is incorrectly formatted.",
            f"It should follow '{VALID_ID_PATTER}'",
            f"Located in {self.id_def.location()}"
        ])


class DuplicityError(NamedTuple):
    id: str
    files: list[IdDef]

    def __str__(self) -> str:
        return '\n\t'.join(
            [f"Duplicit id: {self.id} located in:"]
            + [id_def.location() for id_def in self.files]
        )


ValidationError = DuplicityError | FormatError


class EnumDataList:
    "merges enums with same name"

    def __init__(self) -> None:
        self._enum_by_name: dict[str, EnumData] = {}

    def add(self, enum: EnumData) -> None:
        enum_with_same_name = self._enum_by_name.get(enum.name)
        if enum_with_same_name:
            enum = enum.add(enum_with_same_name)
        self._enum_by_name[enum.name] = enum

    def __iter__(self) -> Generator[EnumData, None, None]:
        yield from self._enum_by_name.values()

    def validate(self) -> Iterator[ValidationError]:
        id_counts: defaultdict[str, list[IdDef]] = defaultdict(list)
        for enum in self:
            for id_def in enum.ids:
                id_counts[id_def.id].append(id_def)
                if not VALID_ID.match(id_def.id):
                    yield FormatError(id_def)
        for id, definitions in id_counts.items():
            if len(definitions) > 1:
                yield DuplicityError(id, definitions)


@contextmanager
def error_context(note: str):
    try:
        yield None
    except Exception as e:
        e.add_note(note)
        raise


def parse_all_xml(xml_file_list: Iterable[Path]) -> dict[Path, EnumDataList]:
    result: dict[Path, EnumDataList] = defaultdict(EnumDataList)
    for xml_file in xml_file_list:
        with error_context(f"Error happened when parsing: {xml_file}"):
            schema_path, data = parse_single_xml(xml_file)
            for enum in data:
                result[schema_path].add(enum)
        print(f"parsed {xml_file} \tOK")
    return result


def generate_all_enums(result: dict[Path, EnumDataList]) -> None:
    for schema_path, enums in result.items():
        schema_name = schema_path.stem
        gen_location = (schema_path.parent
                        / DIRNAME_UTILS
                        / f"{schema_name}-generated.xsd")
        with error_context(
            f"Error happened when generating: {gen_location} (from {schema_path})"
        ):
            generate_single_document(
                enums, target_namespace=schema_name, target_path=gen_location
            )
        print(f"generated {gen_location} \tOK")


def validate_all_enums(result: dict[Path, EnumDataList]) -> bool:
    valid = True
    for error in chain(*(enum_data_list.validate() for enum_data_list in result.values())):
        print(error)
        print()
        valid = False
    return valid


def main(data_dir: Path) -> None:
    print("parsing...")
    output = parse_all_xml(data_dir.glob('*.xml'))
    print("validating...")
    valid = validate_all_enums(output)
    if valid:
        print("removing old generated files...")
        for old in (data_dir/DIRNAME_SCHEMAS/DIRNAME_UTILS).glob('*-generated.xsd'):
            old.unlink()
        print("generating...")
        generate_all_enums(output)
    print("Done.")


if __name__ == "__main__":
    data_dir = Path("public/data")
    if data_dir.is_dir():
        main(data_dir)
    else:
        print(f"Error: {data_dir} not found")
        print(
            "Maybe you are in a wrong folder?",
            "You should run this script from root of the database-public repo:"
        )
        print("  python3 utils/generate_enums.py")
