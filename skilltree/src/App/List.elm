port module App.List exposing (Model, Msg, main)

import Browser
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Decode
import Json.Encode
import Jutsu exposing (Jutsu)
import Tree
import UI
import UI.ErrorList
import Usecase.DarkTheme
import Usecase.JutsuFilter
import Usecase.TechnikyDocument
import Utils
import UI.Loading


main : Program Json.Decode.Value Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions =
            \model ->
                case Usecase.TechnikyDocument.isReady model.document of
                    Usecase.TechnikyDocument.Ready { cache } ->
                        routeReceiverSub cache

                    _ ->
                        Sub.none
        }


type alias Model =
    { filter : Maybe Usecase.JutsuFilter.State
    , document : Usecase.TechnikyDocument.TechnikyDocumentState
    , darkTheme : Usecase.DarkTheme.Model
    }



-- FLAGS / PORTS


port hashParamsEmitter : Json.Decode.Value -> Cmd msg


port hashParamsReceiver : (Json.Decode.Value -> msg) -> Sub msg


routeReceiverSub : DatabaseCache -> Sub Msg
routeReceiverSub cache =
    let
        wrap : Result er (List String) -> Msg
        wrap filterResult =
            case Result.map (Usecase.JutsuFilter.fromRoute cache) filterResult of
                Ok (Just filter) ->
                    ChangedFilterFromOutside (Just filter)

                _ ->
                    ChangedFilterFromOutside Nothing
    in
    hashParamsReceiver
        (Json.Decode.decodeValue routeDecoder >> wrap)


routeDecoder : Json.Decode.Decoder (List String)
routeDecoder =
    Json.Decode.field "route" Json.Decode.string
        |> Json.Decode.map (String.split "_")
        |> Json.Decode.maybe
        |> Json.Decode.map (Maybe.withDefault [])


encodeRoute : List String -> Json.Encode.Value
encodeRoute route =
    Json.Encode.object
        [ ( "route", Json.Encode.string (String.join "_" route) )
        ]


type alias Flags =
    { route : List String
    , darkTheme : Usecase.DarkTheme.Model
    }


defaultFlags : Flags
defaultFlags =
    { route = []
    , darkTheme = Usecase.DarkTheme.withoutTheming
    }


flagsDecoder : Json.Decode.Decoder Flags
flagsDecoder =
    Json.Decode.map2 Flags
        (Json.Decode.field "hashParams" routeDecoder)
        Usecase.DarkTheme.flagsDecoder


decodeFlags : Json.Decode.Value -> Flags
decodeFlags flags =
    Json.Decode.decodeValue flagsDecoder flags
        -- |> Result.mapError (Debug.log "flags decode error")
        |> Result.withDefault defaultFlags



-- INIT


init : Json.Decode.Value -> ( Model, Cmd Msg )
init rawFlags =
    let
        flags : Flags
        flags =
            decodeFlags rawFlags
    in
    ( { document = Usecase.TechnikyDocument.initState
      , filter = Nothing
      , darkTheme = flags.darkTheme
      }
    , Usecase.TechnikyDocument.fetchAll
        |> Cmd.map (TechnikyDocumentMsg flags)
    )



-- UPDATE


type Msg
    = ChangedFilter Usecase.JutsuFilter.State
    | ChangedFilterFromOutside (Maybe Usecase.JutsuFilter.State)
    | TechnikyDocumentMsg Flags Usecase.TechnikyDocument.TechnikyDocumentMsg
    | DarkThemeMsg Usecase.DarkTheme.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChangedFilter filter ->
            ( { model | filter = Just filter }
            , hashParamsEmitter (encodeRoute (Usecase.JutsuFilter.toRoute filter))
            )

        ChangedFilterFromOutside (Just filter) ->
            ( { model | filter = Just filter }
            , Cmd.none
            )

        ChangedFilterFromOutside Nothing ->
            ( model
            , Cmd.none
            )

        TechnikyDocumentMsg flags docMsg ->
            let
                document : Usecase.TechnikyDocument.TechnikyDocumentState
                document =
                    Usecase.TechnikyDocument.updateState docMsg model.document
            in
            ( { model
                | document = document
                , filter =
                    case Usecase.TechnikyDocument.isReady document of
                        Usecase.TechnikyDocument.Ready doc ->
                            Usecase.JutsuFilter.fromRoute doc.cache flags.route
                                |> Maybe.withDefault (Usecase.JutsuFilter.init (Tree.id (Tuple.first doc.trees)))
                                |> Just

                        _ ->
                            model.filter
              }
            , Cmd.none
            )

        DarkThemeMsg darkThemeMsg ->
            Usecase.DarkTheme.update DarkThemeMsg darkThemeMsg model



--VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewBody model
        ]


viewBody : Model -> Html Msg
viewBody model =
    case ( model.filter, Usecase.TechnikyDocument.isReady model.document ) of
        ( Just filter, Usecase.TechnikyDocument.Ready doc ) ->
            Html.div
                []
                [ viewTop model.darkTheme
                , viewFilter doc filter
                , viewHeader doc filter
                , viewTechniky doc filter
                ]

        ( _, Usecase.TechnikyDocument.Error errors ) ->
            UI.ErrorList.view errors

        (_, Usecase.TechnikyDocument.Loading loading ) ->
            UI.Loading.viewFullPage loading

        _ ->
            Html.text "Loading..."


viewTop : Usecase.DarkTheme.Model -> Html Msg
viewTop darkTheme =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 0.5)
            , Css.displayFlex
            , Css.justifyContent Css.spaceBetween
            ]
        ]
        [ UI.minor Utils.versionText
        , Usecase.DarkTheme.view darkTheme
            |> Html.map DarkThemeMsg
        ]


viewHeader : Usecase.TechnikyDocument.TechnikyDocument -> Usecase.JutsuFilter.State -> Html Msg
viewHeader doc filter =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 0.5)
            ]
        ]
        [ Usecase.JutsuFilter.viewHeader doc.cache filter
        ]


viewFilter : Usecase.TechnikyDocument.TechnikyDocument -> Usecase.JutsuFilter.State -> Html Msg
viewFilter doc filter =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.padding (Css.rem 0.5)
            , Css.position Css.sticky
            , Css.top Css.zero
            , Css.zIndex (Css.int 1)
            , Css.border3 (Css.px 1) Css.solid Css.transparent
            , Css.property "border-bottom-color" UI.varColMinorFg
            ]
        ]
        [ Usecase.JutsuFilter.view
            doc.cache
            { onChange = ChangedFilter
            , quirks = doc.specSchopnosti
            , trees = doc.trees
            }
            filter
        ]


viewTechniky : Usecase.TechnikyDocument.TechnikyDocument -> Usecase.JutsuFilter.State -> Html Msg
viewTechniky doc filter =
    let
        filtered : List Jutsu
        filtered =
            doc.trees
                |> Utils.cons
                |> List.concatMap Tree.techniky
                |> List.filter (Usecase.JutsuFilter.matches filter)
                |> List.sortBy Jutsu.level
    in
    if List.length filtered == 0 then
        Html.div
            [ Attrs.css
                [ Css.padding (Css.rem 0.5)
                ]
            ]
            [ UI.minor "Žádné výsledky"
            ]

    else
        Html.div
            [ Attrs.css
                [ Css.padding (Css.rem 0.5)
                , Css.displayFlex
                , Css.flexDirection Css.row
                , Css.flexWrap Css.wrap
                , Css.alignItems Css.flexStart
                , Css.property "gap" "1rem"
                ]
            ]
            (filtered
                |> List.map (Jutsu.viewDetail doc.cache "" True GUID.setEmpty)
            )
