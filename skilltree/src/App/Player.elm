module App.Player exposing (CharState, Model, Msg, main)

import Browser
import Character exposing (Character)
import Css
import DatabaseCache exposing (DatabaseCache)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Decode
import Jutsu
import Platform.Cmd as Cmd
import Tree
import UI
import UI.Columns
import UI.ErrorList
import Usecase.CharacterStats
import Usecase.ExtractHelp
import Usecase.TechnikyDocument
import Utils


main : Program Json.Decode.Value Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always Sub.none
        }



-- FLAGS


type alias Flags =
    { character : DatabaseCache -> Character
    }


flagsDecoder : Json.Decode.Decoder Flags
flagsDecoder =
    Json.Decode.map Flags
        (Json.Decode.field "character" Character.decoderSandbox)


decodeFlags : Json.Decode.Value -> Result Json.Decode.Error Flags
decodeFlags flags =
    Json.Decode.decodeValue flagsDecoder flags



-- INIT / MODEL


init : Json.Decode.Value -> ( Model, Cmd Msg )
init flagsRaw =
    ( { document = Usecase.TechnikyDocument.initState
      , character = CharLoading
      }
    , Usecase.TechnikyDocument.fetchAll
        |> Cmd.map (TechnikyDocumentMsg (decodeFlags flagsRaw))
    )


type alias Model =
    { document : Usecase.TechnikyDocument.TechnikyDocumentState
    , character : CharState
    }


type CharState
    = CharLoading
    | CharError String
    | CharReady Character



-- UPDATE


type Msg
    = TechnikyDocumentMsg (Result Json.Decode.Error Flags) Usecase.TechnikyDocument.TechnikyDocumentMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TechnikyDocumentMsg (Ok flags) docMsg ->
            let
                document : Usecase.TechnikyDocument.TechnikyDocumentState
                document =
                    Usecase.TechnikyDocument.updateState docMsg model.document
            in
            ( { model
                | document =
                    document
                , character =
                    case Usecase.TechnikyDocument.isReady document of
                        Usecase.TechnikyDocument.Ready { cache } ->
                            CharReady (flags.character cache)

                        _ ->
                            model.character
              }
            , Cmd.none
            )

        TechnikyDocumentMsg (Err e) _ ->
            ( { model | character = CharError (Json.Decode.errorToString e) }
            , Cmd.none
            )



-- VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            ]
        ]
        [ UI.Columns.view (viewBody model)
        ]


viewBody : Model -> List (Html Msg)
viewBody model =
    case ( Usecase.TechnikyDocument.isReady model.document, model.character ) of
        ( Usecase.TechnikyDocument.Ready data, CharReady character ) ->
            viewMain data character

        ( Usecase.TechnikyDocument.Error errors, _ ) ->
            [ UI.ErrorList.view errors
            ]

        ( _, CharError er ) ->
            [ UI.ErrorList.view [ er ]
            ]

        _ ->
            [ Html.text "Loading..."
            ]


viewMain : Usecase.TechnikyDocument.TechnikyDocument -> Character -> List (Html msg)
viewMain data character =
    [ Html.div
        []
        [ viewHelp data character
        , UI.space (Css.rem 1)
        , Html.div
            [ Attrs.css
                [ Css.marginBottom (Css.rem 1)
                ]
            ]
            [ Html.h2
                [ Attrs.css
                    [ Css.margin (Css.rem 0)
                    ]
                ]
                [ Html.text "Zvolené techniky"
                ]
            , Usecase.CharacterStats.viewCharacterStats data.cache data.trees character
            ]
        , Tree.viewPickedList data.cache character data.trees
        ]
    ]


viewHelp : Usecase.TechnikyDocument.TechnikyDocument -> Character -> Html msg
viewHelp data character =
    Utils.cons data.trees
        |> List.concatMap Tree.techniky
        |> List.filter (Jutsu.id >> Character.hasTechnika character)
        |> Usecase.ExtractHelp.view data.cache
