module App.Table exposing (Model, Msg, main)

import Browser
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Jutsu exposing (Jutsu)
import JutsuAccess
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI
import UI.ErrorList
import UI.Loading
import Usecase.TechnikyDocument
import Utils


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always Sub.none
        }


type alias Model =
    { document : Usecase.TechnikyDocument.TechnikyDocumentState
    }



-- INIT


init : () -> ( Model, Cmd Msg )
init _ =
    ( { document = Usecase.TechnikyDocument.initState
      }
    , Usecase.TechnikyDocument.fetchAll |> Cmd.map TechnikyDocumentMsg
    )



-- UPDATE


type Msg
    = TechnikyDocumentMsg Usecase.TechnikyDocument.TechnikyDocumentMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TechnikyDocumentMsg docMsg ->
            ( { model | document = Usecase.TechnikyDocument.updateState docMsg model.document }
            , Cmd.none
            )



--VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ UI.normal
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewBody model
        ]


viewBody : Model -> Html Msg
viewBody model =
    case Usecase.TechnikyDocument.isReady model.document of
        Usecase.TechnikyDocument.Ready data ->
            Html.div
                []
                [ viewTechniky data
                , Html.div
                    [ Attrs.css [ UI.textMinor, Css.padding (Css.rem 0.5) ]
                    ]
                    [ Html.text Utils.versionText
                    ]
                ]

        Usecase.TechnikyDocument.Error errors ->
            UI.ErrorList.view errors

        Usecase.TechnikyDocument.Loading loading ->
            UI.Loading.viewFullPage loading


viewTechniky : Usecase.TechnikyDocument.TechnikyDocument -> Html Msg
viewTechniky { cache, trees, quirky } =
    let
        techniky : List Jutsu
        techniky =
            List.concatMap Tree.techniky (Utils.cons trees)
    in
    Html.div
        []
        [ Utils.cons trees
            |> List.concatMap (viewBasic cache)
            |> Jutsu.viewTableContainer
        , quirky
            |> List.filter (not << QuirkMechanicky.neniVeHre)
            |> GUID.listSortBy QuirkMechanicky.id
            |> List.concatMap (viewSpecial cache techniky)
            |> Jutsu.viewTableContainer
        ]


viewSpecial : DatabaseCache -> List Jutsu -> QuirkMechanicky -> List (Html Msg)
viewSpecial cache allJutsu quirk =
    let
        quirkId : GUID.GUID GUID.QuirkMechanicky
        quirkId =
            QuirkMechanicky.id quirk
    in
    allJutsu
        |> List.filter (Jutsu.access >> JutsuAccess.isQuirk)
        |> List.filter (\j -> JutsuAccess.canAccess (GUID.setFromList [ quirkId ]) (Jutsu.access j))
        |> List.sortBy Jutsu.level
        |> List.map (Jutsu.viewTableRow cache Nothing)
        |> ifNotEmpty ((++) (Jutsu.viewTableHeader cache Nothing quirkId Nothing))


viewBasic : DatabaseCache -> Tree -> List (Html Msg)
viewBasic cache tree =
    let
        treeId : GUID.GUID GUID.Tree
        treeId =
            Tree.id tree
    in
    Tree.techniky tree
        |> List.filter (Jutsu.access >> JutsuAccess.isPublic)
        |> List.sortBy Jutsu.level
        |> List.map (Jutsu.viewTableRow cache (Tree.color tree))
        |> ifNotEmpty ((++) (Jutsu.viewTableHeader cache (Tree.color tree) treeId (Tree.pecet tree)))


ifNotEmpty : (List a -> List a) -> List a -> List a
ifNotEmpty fn data =
    if List.length data > 0 then
        fn data

    else
        []
