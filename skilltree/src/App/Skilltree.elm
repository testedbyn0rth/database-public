port module App.Skilltree exposing (CharacterSaving, Model, Msg, main)

import Browser
import Character exposing (Character)
import Css
import DatabaseCache exposing (DatabaseCache)
import Env.Env
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Html.Styled.Events as Events
import Json.Decode
import Json.Encode
import Platform.Cmd as Cmd
import Tree
import UI
import UI.Columns
import UI.ErrorList
import UI.List
import Url.Builder
import Usecase.CharacterConcept as CharacterConcept
import Usecase.CharacterStats
import Usecase.DarkTheme
import Usecase.TechnikyDocument
import Utils
import UI.Loading


main : Program Json.Decode.Value Model Msg
main =
    Browser.element
        { init = init
        , view = Html.toUnstyled << view
        , update = update
        , subscriptions = always characterSandboxReceiverSub
        }



-- FLAGS / PORTS


port hashParamsEmitter : Json.Decode.Value -> Cmd msg


port hashParamsReceiver : (Json.Decode.Value -> msg) -> Sub msg


characterSandboxReceiverSub : Sub Msg
characterSandboxReceiverSub =
    hashParamsReceiver
        (Json.Decode.decodeValue Character.decoderSandbox >> ChangedSandboxCharacter)


type alias Flags =
    { character : FlagsCharacter
    , darkTheme : Usecase.DarkTheme.Model
    }


type FlagsCharacter
    = FlagsFetchCharacter FlagsFetchCharacterPayload
    | FlagsSandbox (DatabaseCache -> Character)


defaultFlags : Flags
defaultFlags =
    { character = FlagsSandbox (always Character.empty)
    , darkTheme = Usecase.DarkTheme.withoutTheming
    }


type alias FlagsFetchCharacterPayload =
    { characterId : Int
    }


flagsDecoderFetchCharacter : Json.Decode.Decoder FlagsCharacter
flagsDecoderFetchCharacter =
    Json.Decode.map FlagsFetchCharacter <|
        Json.Decode.map FlagsFetchCharacterPayload
            (Json.Decode.field "characterId" Json.Decode.int)


flagsDecoderSandbox : Json.Decode.Decoder FlagsCharacter
flagsDecoderSandbox =
    Json.Decode.map FlagsSandbox Character.decoderSandbox


flagsDecoder : Json.Decode.Decoder Flags
flagsDecoder =
    Json.Decode.map2 Flags
        (Json.Decode.field "hashParams" (Json.Decode.oneOf [ flagsDecoderFetchCharacter, flagsDecoderSandbox ]))
        Usecase.DarkTheme.flagsDecoder


decodeFlags : Json.Decode.Value -> Flags
decodeFlags flags =
    Json.Decode.decodeValue flagsDecoder flags
        -- |> Result.mapError (Debug.log "flags decode error")
        |> Result.withDefault defaultFlags



-- INIT / MODEL


init : Json.Decode.Value -> ( Model, Cmd Msg )
init flagsRaw =
    let
        flags : Flags
        flags =
            decodeFlags flagsRaw
    in
    ( { currentCharacter =
            Character.empty
      , characterSaving =
            CharLoading
      , allowSandbox =
            case flags.character of
                FlagsSandbox _ ->
                    True

                _ ->
                    False
      , darkTheme =
            flags.darkTheme
      , conceptState =
            CharacterConcept.emptyState
      , document =
            Usecase.TechnikyDocument.initState
      }
    , Cmd.batch
        [ Usecase.TechnikyDocument.fetchAll
            |> Cmd.map (TechnikyDocumentMsg flags)
        , case flags.character of
            FlagsFetchCharacter payload ->
                Character.fetch payload.characterId GotCharacter

            _ ->
                Cmd.none
        ]
    )


type alias Model =
    { currentCharacter : Character -- TODO: merge currentCharacter with CharacterSaving
    , characterSaving : CharacterSaving
    , allowSandbox : Bool
    , darkTheme : Usecase.DarkTheme.Model
    , conceptState : CharacterConcept.State
    , document : Usecase.TechnikyDocument.TechnikyDocumentState
    }


type CharacterSaving
    = CharLoading
    | CharClean
    | CharDirty
    | CharSaving
    | CharError String



-- UPDATE


type Msg
    = TechnikyDocumentMsg Flags Usecase.TechnikyDocument.TechnikyDocumentMsg
    | GotCharacter (Result String Character)
    | SkilltreeChangedCharacter Character
    | ConceptStateChanged CharacterConcept.State
    | DarkThemeMsg Usecase.DarkTheme.Msg
    | ChangedSandboxCharacter (Result Json.Decode.Error (DatabaseCache -> Character))
    | UserRequestedSave Character
    | SaveCompleted (Result String ())


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TechnikyDocumentMsg flags technikyMsg ->
            let
                document : Usecase.TechnikyDocument.TechnikyDocumentState
                document =
                    Usecase.TechnikyDocument.updateState technikyMsg model.document
            in
            ( { model
                | document = document
                , currentCharacter =
                    case ( flags.character, Usecase.TechnikyDocument.isReady document ) of
                        ( FlagsSandbox sandboxParams, Usecase.TechnikyDocument.Ready { cache } ) ->
                            sandboxParams cache

                        _ ->
                            model.currentCharacter
              }
            , Cmd.none
            )

        GotCharacter (Ok character) ->
            ( { model
                | currentCharacter = character
                , characterSaving = CharClean
              }
            , Cmd.none
            )

        GotCharacter (Err e) ->
            ( { model | characterSaving = CharError e }
            , Cmd.none
            )

        SkilltreeChangedCharacter newChar ->
            ( { model
                | currentCharacter = newChar
                , characterSaving = CharDirty
              }
            , if model.allowSandbox then
                hashParamsEmitter (Character.dump newChar)

              else
                Cmd.none
              -- TODO: autosave?
            )

        DarkThemeMsg darkThemeMsg ->
            Usecase.DarkTheme.update DarkThemeMsg darkThemeMsg model

        ConceptStateChanged state ->
            ( { model | conceptState = state }
            , Cmd.none
            )

        ChangedSandboxCharacter (Ok makeCharacter) ->
            case Usecase.TechnikyDocument.isReady model.document of
                Usecase.TechnikyDocument.Ready { cache } ->
                    ( { model | currentCharacter = makeCharacter cache }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        ChangedSandboxCharacter (Err _) ->
            ( model, Cmd.none )

        UserRequestedSave char ->
            ( { model
                | currentCharacter = char
                , characterSaving = CharSaving
              }
            , Character.save char SaveCompleted
            )

        SaveCompleted (Ok ()) ->
            ( { model
                | characterSaving =
                    if model.characterSaving == CharSaving then
                        CharClean

                    else
                        model.characterSaving
              }
            , Cmd.none
            )

        SaveCompleted (Err _) ->
            ( { model
                | characterSaving =
                    if model.characterSaving == CharSaving then
                        CharDirty

                    else
                        model.characterSaving
              }
            , Cmd.none
            )


charDumpDecoder : Json.Decode.Decoder (List ( String, String ))
charDumpDecoder =
    Json.Decode.keyValuePairs <|
        Json.Decode.oneOf
            [ Json.Decode.string
            , Json.Decode.value
                |> Json.Decode.map (Json.Encode.encode 0)
            ]


dumpToUrl : Json.Decode.Value -> String
dumpToUrl dump =
    let
        hash : String
        hash =
            Json.Decode.decodeValue charDumpDecoder dump
                |> Result.withDefault []
                |> List.map (\( k, v ) -> Url.Builder.string k v)
                |> Url.Builder.toQuery
                |> String.slice 1 99999
    in
    Url.Builder.custom
        Env.Env.environment.databasePublicRoot
        (Env.Env.environment.databasePublicPathBase
            ++ [ "skilltree.html" ]
        )
        []
        (Just hash)



-- VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Attrs.css
            [ Css.marginBottom (Css.rem 50)
            , UI.normal
            , Css.padding Css.zero
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ UI.Columns.view (viewBody model)
        ]


viewBody : Model -> List (Html Msg)
viewBody model =
    case Usecase.TechnikyDocument.isReady model.document of
        Usecase.TechnikyDocument.Ready { trees, quirky, cache } ->
            [ Html.div
                [ Attrs.css
                    [ Css.flexGrow (Css.num 1)
                    , Css.flexBasis (Css.rem 20)
                    , Css.maxWidth (Css.pct 100)
                    ]
                ]
                [ viewStickySaveButton model
                , UI.List.view
                    []
                    [ UI.row
                        [ Attrs.css
                            [ Css.padding (Css.rem 0.5)
                            ]
                        ]
                        [ viewForeword model.currentCharacter model.allowSandbox
                        , Usecase.DarkTheme.view model.darkTheme
                            |> Html.map DarkThemeMsg
                        ]
                    , CharacterConcept.view cache
                        { onCharacter = SkilltreeChangedCharacter
                        , trees = trees
                        , quirks = quirky
                        , character = model.currentCharacter
                        , allowChanges = model.allowSandbox
                        , state = model.conceptState
                        , onState = ConceptStateChanged
                        }
                    , Tree.viewSkilltree
                        cache
                        { character = model.currentCharacter
                        , onCharacter = SkilltreeChangedCharacter
                        }
                        trees
                    ]
                ]
            , Html.div
                [ Attrs.css
                    [ Css.flexBasis (Css.rem 30)
                    , Css.padding (Css.rem 0.5)
                    ]
                ]
                [ Html.div
                    [ Attrs.css
                        [ Css.marginBottom (Css.rem 1)
                        ]
                    ]
                    [ Html.h2
                        [ Attrs.css
                            [ Css.margin (Css.rem 0)
                            ]
                        ]
                        [ Html.text "Zvolené techniky"
                        ]
                    , Usecase.CharacterStats.viewCharacterStats cache trees model.currentCharacter
                    ]
                , Tree.viewPickedList cache model.currentCharacter trees
                ]
            ]

        Usecase.TechnikyDocument.Loading loading ->
            [ UI.Loading.viewFullPage loading
            ]

        Usecase.TechnikyDocument.Error errors ->
            [ UI.ErrorList.view errors
            ]


viewStickySaveButton : Model -> Html Msg
viewStickySaveButton model =
    let
        ( disabled, text ) =
            case model.characterSaving of
                CharLoading ->
                    ( True, "Načítání..." )

                CharClean ->
                    ( True, "Uloženo" )

                CharDirty ->
                    ( False, "Uložit" )

                CharSaving ->
                    ( True, "Ukládání..." )

                CharError err ->
                    ( True, err )
    in
    UI.whenHtml (not model.allowSandbox) <|
        Html.div
            [ Attrs.css
                [ Css.position Css.sticky
                , Css.top Css.zero
                , Css.zIndex (Css.int 2)
                , Css.height Css.zero
                , Css.displayFlex
                , Css.alignItems Css.start
                , Css.justifyContent Css.end
                ]
            ]
            [ Html.button
                [ Attrs.type_ "button"
                , Events.onClick (UserRequestedSave model.currentCharacter)
                , Attrs.disabled disabled
                ]
                [ Html.text text
                ]
            ]


viewForeword : Character -> Bool -> Html Msg
viewForeword char allowSandbox =
    Html.div
        [ Attrs.css [ UI.textMinor ]
        ]
        [ Html.text "Pro pochopení technik doporučujeme přečíst si "
        , Html.a
            [ Attrs.css [ UI.textMinor ]
            , Attrs.href "https://narutolarp.cz/rules.php#rules_3"
            , Attrs.target "_blank"
            ]
            [ Html.text "Pravidla."
            ]
        , Html.br [] []
        , if allowSandbox then
            Html.text "Pro sdílení buildu stačí zkopírovat url z adresního řádku prohlížeče."

          else
            Html.span
                []
                [ Html.text "Pro sdílení svých zrovna naklikaných schopností zkopírujte tento "
                , Html.a
                    [ Attrs.css [ UI.textMinor ]
                    , Attrs.href (dumpToUrl (Character.dump char))
                    , Attrs.target "_blank"
                    ]
                    [ Html.text "odkaz na zvolené techniky"
                    ]
                ]
        , Html.br [] []
        , Html.text Utils.versionText
        ]
