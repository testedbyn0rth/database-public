module Env.Env exposing (..)

import Env.Util.TypeDefinition
import Url.Builder


environment : Env.Util.TypeDefinition.Environment
environment =
    { databasePublicRoot = Url.Builder.Relative
    , databasePublicPathBase = []
    }
