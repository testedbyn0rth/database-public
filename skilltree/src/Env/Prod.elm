module Env.Prod exposing (..)

import Env.Util.BuildVariables
import Env.Util.TypeDefinition
import Url.Builder


environment : Env.Util.TypeDefinition.Environment
environment =
    { databasePublicRoot = Url.Builder.Absolute
    , databasePublicPathBase = [ "database-public", Env.Util.BuildVariables.version ]
    }
