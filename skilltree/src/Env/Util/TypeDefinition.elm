module Env.Util.TypeDefinition exposing (..)

import Url.Builder


type alias Environment =
    { databasePublicRoot : Url.Builder.Root
    , databasePublicPathBase : List String
    }
