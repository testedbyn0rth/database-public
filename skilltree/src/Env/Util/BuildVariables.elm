module Env.Util.BuildVariables exposing (..)


version : String
version =
    "version"


versionDate : String
versionDate =
    "1970-01-01"
