module Usecase.TechnikyDocument exposing (IsReady(..), TechnikyDocument, TechnikyDocumentMsg, TechnikyDocumentState, fetchAll, initState, isReady, updateState)

import Api
import DatabaseCache exposing (DatabaseCache)
import Env.Env
import Env.Util.BuildVariables
import GUID exposing (GUID)
import QuirkMechanicky exposing (QuirkMechanicky)
import Task
import Tree exposing (Tree)
import Url.Builder
import Utils exposing (ListNotEmpty, listNotEmpty)
import Xml.Decode


type alias TechnikyDocument =
    { trees : ListNotEmpty Tree
    , quirky : List QuirkMechanicky
    , specSchopnosti : ListNotEmpty QuirkMechanicky
    , cache : DatabaseCache
    }


type IsReady
    = Ready TechnikyDocument
    | Loading ( Int, Int )
    | Error (List String)


isReady : TechnikyDocumentState -> IsReady
isReady (TechnikyDocumentState state) =
    if not (List.isEmpty state.errors) then
        Error state.errors

    else
        let
            loadingCondition : Bool
            loadingCondition =
                (List.length state.stromy == List.length xmlTreeDocumentsToFetch)
                    && state.definiceFinished
        in
        case ( state.quirky, loadingCondition, listNotEmpty state.stromy ) of
            ( Just quirky, True, Just stromy ) ->
                case listNotEmpty quirky of
                    Just spec ->
                        Ready
                            { trees = Tree.sort stromy
                            , quirky = quirky
                            , specSchopnosti = spec
                            , cache = state.cache
                            }

                    Nothing ->
                        Error [ "Nenacetly se specialni schopnosti" ]

            _ ->
                Loading
                    ( List.length state.stromy
                        + Utils.boolToInt state.definiceFinished
                        + Utils.boolToInt (state.quirky /= Nothing)
                    , List.length xmlTreeDocumentsToFetch
                        + 2
                    )


type TechnikyDocumentState
    = TechnikyDocumentState
        { stromy : List Tree
        , quirky : Maybe (List QuirkMechanicky)
        , cache : DatabaseCache
        , definiceFinished : Bool
        , errors : List String
        }


initState : TechnikyDocumentState
initState =
    TechnikyDocumentState
        { stromy = []
        , quirky = Nothing
        , cache = initCache
        , definiceFinished = False
        , errors = []
        }


type TechnikyDocumentMsg
    = MsgTree (Result String Tree)
    | MsgSpec (Result String (List QuirkMechanicky))
    | MsgDefinice (Result String DatabaseCache.MapCache)


updateState : TechnikyDocumentMsg -> TechnikyDocumentState -> TechnikyDocumentState
updateState msg (TechnikyDocumentState state) =
    TechnikyDocumentState <|
        case msg of
            MsgTree (Result.Err err) ->
                { state | errors = err :: state.errors }

            MsgTree (Result.Ok strom) ->
                { state
                    | stromy = strom :: state.stromy
                    , cache = DatabaseCache.registerIterHelper Tree.updateCache [ strom ] state.cache
                }

            MsgSpec (Result.Err err) ->
                { state | errors = err :: state.errors }

            MsgSpec (Result.Ok quirky) ->
                { state
                    | quirky = Just quirky
                    , cache = DatabaseCache.registerIterHelper QuirkMechanicky.updateCache quirky state.cache
                }

            MsgDefinice (Result.Err err) ->
                { state | errors = err :: state.errors }

            MsgDefinice (Result.Ok updateCache) ->
                { state
                    | cache = updateCache state.cache
                    , definiceFinished = True
                }



--------------------------


definicePaths =
    [ ( "zásahy", "zásah", DatabaseCache.commonDecoder )
    , ( "hesla", "ofenzivní", DatabaseCache.commonDecoder )
    , ( "hesla", "defenzivní", DatabaseCache.commonDecoder )
    , ( "atributy", "atribut", DatabaseCache.atributDecoder )
    , ( "ceny", "cena", DatabaseCache.commonDecoder )
    , ( "cooldowny", "cooldown", DatabaseCache.commonDecoder )
    , ( "typy-vnímání", "typ-vnímání", DatabaseCache.commonDecoder )
    , ( "klíčová-slova", "klíčové-slovo", DatabaseCache.commonDecoder )
    , ( "typy-efektu", "typ-efektu", DatabaseCache.commonDecoder )
    , ( "modifikátory-efektu", "modifikátor-efektu", DatabaseCache.commonDecoder )
    , ( "elementy", "element", DatabaseCache.elementDecoder )

    -- , ( "pečeť-definice", "pečeť" ) TODO <pečeť id="pes" obrázek="dog" />
    ]


fetchAll : Cmd TechnikyDocumentMsg
fetchAll =
    Task.attempt MsgDefinice fetchDefinice
        :: Task.attempt MsgSpec fetchSpecialniSchopnosti
        :: List.map (Task.attempt MsgTree << fetchTree) xmlTreeDocumentsToFetch
        |> Cmd.batch


xmlUrlWithCachePunch : List String -> String
xmlUrlWithCachePunch path =
    Url.Builder.custom
        Env.Env.environment.databasePublicRoot
        (Env.Env.environment.databasePublicPathBase
            ++ ("data" :: path)
        )
        [ Url.Builder.string "v" Env.Util.BuildVariables.version
        ]
        Nothing


fetchDefinice =
    Api.fetch
        { url = xmlUrlWithCachePunch [ "definice.xml" ]
        , decoder = cacheDecoder definicePaths
        }


fetchSpecialniSchopnosti =
    Api.fetch
        { url = xmlUrlWithCachePunch [ "specialni-schopnosti.xml" ]
        , decoder = Xml.Decode.path [ "quirk-mechanicky" ] (Xml.Decode.list QuirkMechanicky.decoder)
        }


fetchTree fileName =
    Api.fetch
        { url = xmlUrlWithCachePunch [ fileName ]
        , decoder = Tree.decoder
        }


xmlTreeDocumentsToFetch : List String
xmlTreeDocumentsToFetch =
    [ "strom-Fuinjutsu.xml"
    , "strom-Keitai.xml"
    , "strom-yang-styl.xml"
    , "strom-Fuuton.xml"
    , "strom-Ningu.xml"
    , "strom-yin-styl.xml"
    , "strom-Jinton.xml"
    , "strom-Raiton.xml"
    , "strom-Bukijutsu.xml"
    , "strom-Kakuran.xml"
    , "strom-Suiton.xml"
    , "strom-Doton.xml"
    , "strom-Katon.xml"
    , "strom-Taijutsu.xml"
    ]



-- CACHE


initCache : DatabaseCache
initCache =
    DatabaseCache.empty
        |> DatabaseCache.registerIterHelper updateCacheVillage_hardcoded GUID.const.villages


updateCacheVillage_hardcoded : GUID GUID.Village -> DatabaseCache -> DatabaseCache
updateCacheVillage_hardcoded id =
    DatabaseCache.registerCommon id
        { name = GUID.villageName_TODO id
        , details = ""
        , alias = Nothing
        }


cacheDecoder :
    List ( String, String, String -> Xml.Decode.Decoder DatabaseCache.MapCache )
    -> Xml.Decode.Decoder DatabaseCache.MapCache
cacheDecoder tagNames =
    case tagNames of
        ( tag, childTag, decoder ) :: rest ->
            Xml.Decode.with
                (Xml.Decode.path [ tag ] (Xml.Decode.single (decoder childTag)))
                (\updateCache -> Xml.Decode.map ((>>) updateCache) (cacheDecoder rest))

        [] ->
            Xml.Decode.succeed identity
