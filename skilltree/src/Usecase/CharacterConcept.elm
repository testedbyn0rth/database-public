module Usecase.CharacterConcept exposing (Props, State, emptyState, view)

import Character exposing (Character)
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import QuirkMechanicky exposing (QuirkMechanicky)
import Tree exposing (Tree)
import UI.GridWithDialog
import Utils exposing (ListNotEmpty)


type alias State =
    { activeTab : String
    }


type alias Props msg =
    { character : Character
    , onCharacter : Character -> msg
    , state : State
    , onState : State -> msg
    , trees : ListNotEmpty Tree
    , quirks : List QuirkMechanicky
    , allowChanges : Bool
    }


view : DatabaseCache -> Props msg -> Html msg
view cache props =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.property "gap" "1rem"
            , Css.padding (Css.rem 0.5)
            ]
        ]
        [ Character.viewBasicConcept cache props.allowChanges props.onCharacter props.character
        , viewSpecialniSchopnosti cache props
        , Character.viewNeschopnosti cache props.allowChanges (Tree.neschopnosti props.trees) props.onCharacter props.character
        ]



-- VIEW KONCEPT / SPECIALNI SCHOPNOSTI


viewSpecialniSchopnosti : DatabaseCache -> Props msg -> Html msg
viewSpecialniSchopnosti cache props =
    let
        dialogMode : UI.GridWithDialog.DialogMode QuirkMechanicky msg
        dialogMode =
            if props.allowChanges then
                UI.GridWithDialog.Openable
                    { openLabel = "+ Přidat speciální schopnost ze seznamu"
                    , tabProps =
                        Just
                            { onSelect = props.onState << State
                            , selected = props.state.activeTab
                            , label = getLabel cache
                            }
                    }

            else
                UI.GridWithDialog.NonOpenable
    in
    UI.GridWithDialog.view
        { title = "Speciální schopnosti"
        , dialogMode = dialogMode
        }
        { viewLabel = QuirkMechanicky.name
        , viewDetail = QuirkMechanicky.viewDetail
        , isSelected = QuirkMechanicky.id >> GUID.setHas (Character.quirksMechanicke props.character)
        , isFullWidth = always False
        , editable = props.allowChanges
        , onChange =
            \quirk select ->
                props.onCharacter (Character.setSpecialniSchopnost (QuirkMechanicky.id quirk) select props.character)
        }
        props.quirks


getLabel : DatabaseCache -> QuirkMechanicky -> ListNotEmpty String
getLabel cache quirk =
    QuirkMechanicky.tabLabel_TODO cache quirk


emptyState : State
emptyState =
    { activeTab = QuirkMechanicky.tabLabelVerejne_TODO
    }
