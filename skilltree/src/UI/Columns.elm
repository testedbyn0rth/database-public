module UI.Columns exposing (view)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs


view : List (Html msg) -> Html msg
view items =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexWrap Css.wrap
            , Css.property "gap" "1rem"
            ]
        ]
        items
