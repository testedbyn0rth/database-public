module UI.Loading exposing (Model, viewFullPage)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import UI
import Utils


type alias Model =
    ( Int, Int )


viewFullPage : Model -> Html msg
viewFullPage model =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 1)
            , Css.displayFlex
            , Css.alignItems Css.center
            , Css.justifyContent Css.center
            , Css.minHeight (Css.vh 70)
            , Css.width (Css.vw 100)
            , Css.boxSizing Css.borderBox
            ]
        ]
        [ view model
        ]


view : Model -> Html msg
view ( completed, total ) =
    Html.label
        [ Attrs.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.alignItems Css.center
            , Css.property "gap" "0.2rem"
            , Css.fontSize (Css.rem 1.5)
            ]
        ]
        [ Html.text "Loading"
        , Html.progress
            [ Attrs.max (String.fromInt total)
            , Attrs.value (String.fromInt completed)
            ]
            []
        , UI.minor Utils.versionText
        ]
