module Utils exposing (..)

import Array
import Env.Util.BuildVariables
import List.Extra


justIfNotEmptyString : String -> Maybe String
justIfNotEmptyString s =
    case s of
        "" ->
            Nothing

        notEmpty ->
            Just notEmpty


boolToInt : Bool -> Int
boolToInt bool =
    if bool then
        1

    else
        0



-- NON-EMPTY LIST


type alias ListNotEmpty a =
    ( a, List a )


{-| If not found, fallback to the first element (head)
-}
alwaysFind : (a -> Bool) -> ListNotEmpty a -> a
alwaysFind predicate ( head, rest ) =
    List.Extra.find predicate rest
        |> Maybe.withDefault head


alwaysSort : (a -> comparable) -> ListNotEmpty a -> ListNotEmpty a
alwaysSort key ( head, rest ) =
    List.sortBy key (head :: rest)
        |> listNotEmpty
        |> Maybe.withDefault ( head, [] )


cons : ListNotEmpty a -> List a
cons ( head, rest ) =
    head :: rest


listNotEmpty : List a -> Maybe (ListNotEmpty a)
listNotEmpty list =
    case list of
        head :: tail ->
            Just ( head, tail )

        [] ->
            Nothing



-- NARUTO


levelToLetter : Int -> String
levelToLetter lvl =
    let
        levelLetters : Array.Array String
        levelLetters =
            Array.fromList [ "F", "E", "D", "C", "B", "A", "S", "S+", "S+", "S+" ]
    in
    Array.get lvl levelLetters
        |> Maybe.withDefault ""


type Vsechny a
    = Vsechny
    | Prave a



-- INFRASTRUCTURE


versionText : String
versionText =
    "Verze: "
        ++ Env.Util.BuildVariables.version
        ++ " ("
        ++ Env.Util.BuildVariables.versionDate
        ++ ")"
