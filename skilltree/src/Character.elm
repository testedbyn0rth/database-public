module Character exposing
    ( Character
    , decoderSandbox
    , dump
    , empty
    , fetch
    , hasElement
    , hasNeschopnost
    , hasTechnika
    , level
    , quirksMechanicke
    , save
    , setSpecialniSchopnost
    , setTechnika
    , viewBasicConcept
    , viewNeschopnosti
    , zbyvajiciXP
    , zvolenoTechnik
    )

import Api
import Css
import DatabaseCache exposing (DatabaseCache)
import GUID exposing (GUID, GUIDSet)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attrs
import Json.Decode
import Json.Encode
import UI
import UI.Checkbox
import UI.Definition
import UI.GridWithDialog
import Url.Builder
import Utils
import Xml.Decode
import XmlParser
import Task exposing (Task)


levelSensei : number
levelSensei =
    6


levelNormal : number
levelNormal =
    5


type Character
    = Character CharacterPayload


type alias CharacterPayload =
    { id : Int
    , primaryElement : Maybe (GUID GUID.Element)
    , quirksMechanicke : GUIDSet GUID.QuirkMechanicky
    , neschopnosti : GUIDSet GUID.Neschopnost
    , zapsaneTechniky : GUIDSet GUID.Jutsu
    , zapsaneComment : String
    , level : Int
    }


empty : Character
empty =
    Character
        { id = -1
        , primaryElement = Nothing
        , quirksMechanicke = GUID.setEmpty
        , neschopnosti = GUID.setEmpty
        , zapsaneTechniky = GUID.setEmpty
        , zapsaneComment = ""
        , level = levelNormal
        }


level : Character -> Int
level (Character char) =
    char.level


{-| pocet xp == pocet technik (kazda technika stoji 1 xp)
-}
xp : Character -> Int
xp (Character char) =
    char.level * 4 - 3


zbyvajiciXP : Character -> Int
zbyvajiciXP char =
    xp char - zvolenoTechnik char


zvolenoTechnik : Character -> Int
zvolenoTechnik (Character char) =
    GUID.setLength char.zapsaneTechniky


quirksMechanicke : Character -> GUIDSet GUID.QuirkMechanicky
quirksMechanicke (Character char) =
    char.quirksMechanicke


setSpecialniSchopnost : GUID GUID.QuirkMechanicky -> Bool -> Character -> Character
setSpecialniSchopnost quirk isSelected (Character char) =
    Character
        { char
            | quirksMechanicke = GUID.setAssign char.quirksMechanicke quirk isSelected
        }


hasNeschopnost : Character -> GUID (GUID.NeschopnostAny a) -> Bool
hasNeschopnost (Character char) id =
    GUID.setHas char.neschopnosti (GUID.toNeschopnost id)


hasTechnika : Character -> GUID GUID.Jutsu -> Bool
hasTechnika (Character char) jutsuId =
    GUID.setHas char.zapsaneTechniky jutsuId


hasElement : DatabaseCache -> Character -> GUID GUID.Element -> Bool
hasElement cache char id =
    GUID.setHas (element cache char) id


addTechnika : GUID GUID.Jutsu -> Character -> Character
addTechnika jutsuId (Character char) =
    Character
        { char | zapsaneTechniky = GUID.setAdd jutsuId char.zapsaneTechniky }


removeTechnika : GUID GUID.Jutsu -> Character -> Character
removeTechnika jutsuId (Character char) =
    Character
        { char | zapsaneTechniky = GUID.setRemove jutsuId char.zapsaneTechniky }


setTechnika : GUID GUID.Jutsu -> Character -> Bool -> Character
setTechnika jutsuId character select =
    if select then
        addTechnika jutsuId character

    else
        removeTechnika jutsuId character


hasKekkeiGenkai : DatabaseCache -> Character -> Bool
hasKekkeiGenkai cache (Character char) =
    GUID.setToList char.quirksMechanicke
        |> List.filterMap (DatabaseCache.quirkPayload cache)
        |> List.any .kekkeiGenkai


extractElement : (DatabaseCache.QuirkPayload -> Bool) -> DatabaseCache -> CharacterPayload -> GUIDSet GUID.Element
extractElement filter cache char =
    GUID.setToList char.quirksMechanicke
        |> List.filterMap (DatabaseCache.quirkPayload cache)
        |> List.filter filter
        |> List.map .element
        |> List.foldl GUID.setUnion GUID.setEmpty


primaryElement : DatabaseCache -> Character -> GUIDSet GUID.Element
primaryElement cache (Character char) =
    let
        elementsFromKekkeiGenkai : GUIDSet GUID.Element
        elementsFromKekkeiGenkai =
            extractElement .kekkeiGenkai cache char
    in
    if GUID.setLength elementsFromKekkeiGenkai > 0 then
        elementsFromKekkeiGenkai

    else
        GUID.setFromMaybe char.primaryElement


secondaryElement : DatabaseCache -> Character -> GUIDSet GUID.Element
secondaryElement cache (Character char) =
    extractElement (not << .kekkeiGenkai) cache char


element : DatabaseCache -> Character -> GUIDSet GUID.Element
element cache char =
    GUID.setUnion
        (primaryElement cache char)
        (secondaryElement cache char)



-- SANDBOX


type alias SandboxOptions =
    { jutsu : DatabaseCache -> List (GUID GUID.Jutsu)
    , specialni : DatabaseCache -> List (GUID GUID.QuirkMechanicky)
    , neschopnosti : DatabaseCache -> List (GUID GUID.Neschopnost)
    , element : DatabaseCache -> Maybe (GUID GUID.Element)
    , level : Int
    }


createSandboxCharacter : SandboxOptions -> DatabaseCache -> Character
createSandboxCharacter options cache =
    case empty of
        Character payload ->
            Character
                { payload
                    | zapsaneTechniky =
                        GUID.setFromList (options.jutsu cache)
                    , neschopnosti =
                        GUID.setFromList (options.neschopnosti cache)
                    , level =
                        options.level
                    , quirksMechanicke =
                        GUID.setFromList (options.specialni cache)
                    , primaryElement =
                        options.element cache
                }


optionalIdsField : String -> Json.Decode.Decoder (DatabaseCache -> List (GUID a)) -> Json.Decode.Decoder (DatabaseCache -> List (GUID a))
optionalIdsField fieldName listDecoder =
    optionalFieldJson fieldName listDecoder (always [])


optionalFieldJson : String -> Json.Decode.Decoder a -> a -> Json.Decode.Decoder a
optionalFieldJson fieldName decoder_ default =
    Json.Decode.field fieldName decoder_
        |> Json.Decode.maybe
        |> Json.Decode.map (Maybe.withDefault default)


decoderSandbox : Json.Decode.Decoder (DatabaseCache -> Character)
decoderSandbox =
    Json.Decode.map createSandboxCharacter <|
        Json.Decode.map5 SandboxOptions
            (optionalIdsField "jutsu" DatabaseCache.simpleJutsuListDecoderJsonUntrusted)
            (optionalIdsField "specialni" DatabaseCache.simpleListDecoderJsonUntrustedSpecialni)
            (optionalIdsField "neschopnosti" DatabaseCache.simpleNeschopnostListDecoderJsonUntrusted)
            (optionalFieldJson "element" DatabaseCache.elementDecoderJsonUntrusted (always Nothing))
            (optionalFieldJson "level" intFromStringDecoder (level empty))


intFromStringDecoder : Json.Decode.Decoder Int
intFromStringDecoder =
    Json.Decode.string
        |> Json.Decode.map String.toInt
        |> Json.Decode.andThen
            (Maybe.map Json.Decode.succeed
                >> Maybe.withDefault (Json.Decode.fail "not a number")
            )


dump : Character -> Json.Decode.Value
dump (Character char) =
    let
        encodeSet : GUIDSet t -> Json.Encode.Value
        encodeSet =
            GUID.setToList >> GUID.encodeSimpleListJson
    in
    Json.Encode.object
        [ ( "jutsu", encodeSet char.zapsaneTechniky )
        , ( "specialni", encodeSet char.quirksMechanicke )
        , ( "neschopnosti", encodeSet char.neschopnosti )
        , ( "level", Json.Encode.int char.level )
        , ( "element", Maybe.map GUID.encodeJson char.primaryElement |> Maybe.withDefault Json.Encode.null )
        ]



-- API


fetch : Int -> (Result String Character -> msg) -> Cmd msg
fetch characterId msg =
    Api.fetch
        { url =
            Url.Builder.absolute
                [ "profile", "api-xml.php" ]
                [ Url.Builder.string "subject" "me_schopnosti"
                , Url.Builder.int "_char_id" characterId
                ]
        , decoder = decoder
        }
        |> Task.attempt msg


decoder : Xml.Decode.Decoder Character
decoder =
    Xml.Decode.map Character payloadDecoder


payloadDecoder : Xml.Decode.Decoder CharacterPayload
payloadDecoder =
    -- TODO: validate GUID against DatabaseCache
    Xml.Decode.succeed CharacterPayload
        |> Xml.Decode.requiredPath [ "entity", "id" ] (Xml.Decode.single Xml.Decode.int)
        |> Xml.Decode.requiredPath [ "entity", "primary_element" ] (Xml.Decode.single (Xml.Decode.andThen oneOrZero GUID.simpleListDecoder))
        |> Xml.Decode.requiredPath [ "entity", "quirks_mechanicke" ] (Xml.Decode.single GUID.simpleListDecoderToSet)
        |> Xml.Decode.requiredPath [ "entity", "quirks_mechanicke_zamknute_stromy" ] (Xml.Decode.single GUID.simpleListDecoderToSet)
        |> Xml.Decode.requiredPath [ "entity", "zapsane_techniky" ] (Xml.Decode.single GUID.simpleListDecoderToSet)
        |> Xml.Decode.optionalPath [ "entity", "zapsane_comment" ] (Xml.Decode.single Xml.Decode.string) ""
        |> Xml.Decode.requiredPath [ "entity", "is_sensei" ] (Xml.Decode.single isSenseiLevelDecoder)


isSenseiLevelDecoder : Xml.Decode.Decoder Int
isSenseiLevelDecoder =
    Xml.Decode.string
        |> Xml.Decode.map
            (\isSensei ->
                if isSensei == "true" then
                    levelSensei

                else
                    levelNormal
            )


oneOrZero : List (GUID t) -> Xml.Decode.Decoder (Maybe (GUID t))
oneOrZero list =
    case list of
        [] ->
            Xml.Decode.succeed Nothing

        [ one ] ->
            Xml.Decode.succeed (Just one)

        _ ->
            Xml.Decode.fail "Too many items in oneOrZero"


save : Character -> (Result String () -> msg) -> Cmd msg
save (Character char) msg =
    Api.send
        { url =
            Url.Builder.absolute
                [ "profile", "api-xml.php" ]
                [ Url.Builder.string "subject" "me_schopnosti"
                , Url.Builder.int "_char_id" char.id
                ]
        , decoder = Xml.Decode.succeed ()
        }
        (encode char)
        |> Task.attempt msg


encode : CharacterPayload -> String
encode char =
    XmlParser.format
        { processingInstructions = []
        , docType = Nothing
        , root =
            XmlParser.Element "root"
                []
                [ XmlParser.Element "entity"
                    []
                    [ GUID.encodeSimpleList "zapsane_techniky" (GUID.setToList char.zapsaneTechniky)
                    , XmlParser.Element "zapsane_comment" [] [ XmlParser.Text char.zapsaneComment ]
                    ]
                ]
        }



-- VIEW KONCEPT / BASIC


viewBasicConcept : DatabaseCache -> Bool -> (Character -> msg) -> Character -> Html msg
viewBasicConcept cache allowChanges msg (Character char) =
    Html.div
        [ Attrs.css
            [ Css.displayFlex
            , Css.property "gap" "1rem"
            , Css.alignItems Css.center
            , Css.minHeight (Css.rem 3)
            ]
        ]
        [ UI.Definition.stringLabeled "Úroveň postavy"
            (Just (Utils.levelToLetter char.level))
        , UI.whenHtml (allowChanges || char.level > level empty) <|
            UI.Checkbox.view
                { label = "Jsem sensei"
                , value = char.level > level empty
                , editability =
                    if allowChanges then
                        UI.Checkbox.Editable

                    else
                        UI.Checkbox.Disabled
                , onChange = \isSensei -> msg (Character { char | level = senseiToLevel isSensei })
                }
        , viewPrimaryElement cache allowChanges (Character >> msg) char
        ]


viewPrimaryElement : DatabaseCache -> Bool -> (CharacterPayload -> msg) -> CharacterPayload -> Html msg
viewPrimaryElement cache allowChanges msg char =
    Html.div
        []
        [ UI.Definition.viewLabel "Elementální typ chakry"
        , if hasKekkeiGenkai cache (Character char) then
            DatabaseCache.viewLabeledListShort "," "kekkei-genkai" cache (GUID.setToList (primaryElement cache (Character char)))

          else
            viewPrimaryElementSelect cache allowChanges msg char
        ]


viewPrimaryElementSelect : DatabaseCache -> Bool -> (CharacterPayload -> msg) -> CharacterPayload -> Html msg
viewPrimaryElementSelect cache allowChanges msg char =
    let
        setPrimary : GUID GUID.Element -> msg
        setPrimary elem =
            msg { char | primaryElement = Just elem }
    in
    GUID.viewHtmlSelect
        { options = DatabaseCache.elements cache
        , disabled = not allowChanges
        , onChange = setPrimary
        }
        char.primaryElement


senseiToLevel : Bool -> Int
senseiToLevel isSensei =
    if isSensei then
        level empty + 1

    else
        level empty



-- VIEW KONCEPT / NESCHOPNOSTI


viewNeschopnosti : DatabaseCache -> Bool -> List (GUID GUID.Neschopnost) -> (Character -> msg) -> Character -> Html msg
viewNeschopnosti cache allowChanges neschopnosti onChange (Character char) =
    let
        setNeschopnosti : GUID GUID.Neschopnost -> Bool -> msg
        setNeschopnosti neschopnost isSelected =
            { char | neschopnosti = GUID.setAssign char.neschopnosti neschopnost isSelected }
                |> Character
                |> onChange

        dialogMode : UI.GridWithDialog.DialogMode (GUID GUID.Neschopnost) msg
        dialogMode =
            if allowChanges then
                UI.GridWithDialog.Openable
                    { openLabel = "+ Přidat neschopnost ze seznamu"
                    , tabProps = Nothing
                    }

            else
                UI.GridWithDialog.NonOpenable
    in
    UI.GridWithDialog.view
        { title = "Neschopnosti"
        , dialogMode = dialogMode
        }
        { viewLabel = viewNeschopnostLabel cache
        , viewDetail = viewNeschopnostDetail cache
        , isSelected = GUID.setHas char.neschopnosti
        , isFullWidth = GUID.equal GUID.const.neschopnostElementy
        , editable = allowChanges
        , onChange = setNeschopnosti
        }
        neschopnosti


viewNeschopnostLabel : DatabaseCache -> GUID GUID.Neschopnost -> String
viewNeschopnostLabel cache id =
    if GUID.equal GUID.const.neschopnostElementy id then
        "Neschopný v přeměně podstaty"

    else
        case DatabaseCache.name cache id of
            Just name ->
                "Neschopný: " ++ name

            Nothing ->
                ""


viewNeschopnostDetail : DatabaseCache -> GUID GUID.Neschopnost -> Html msg
viewNeschopnostDetail cache id =
    Html.div
        [ Attrs.css
            [ Css.padding (Css.rem 0.5)
            ]
        ]
    <|
        if GUID.equal GUID.const.neschopnostElementy id then
            [ Html.text "Zamyká techniky druhé a vyšší úrovně ve všech elementálních stromech (5 stromů)"
            ]

        else
            [ UI.minor "Zamyká techniky druhé a vyšší úrovně v tomto stromě:"
            , UI.space (Css.rem 0.2)
            , DatabaseCache.viewCommonBlock cache id
            ]
