cp src/Env/Env.elm src/Env/tmp-backup-Env.elm
cp src/Env/Util/BuildVariables.elm src/Env/Util/tmp-backup-BuildVariables.elm


####################
# BUILD PARAMETERS #

# first param is the elm executable (path or command)
if [ "$1" = "" ]
then
    ELM="elm"
else
    ELM="$1"
fi

# second param can toggle production/staging build
if [ "$2" = "prod" ]
then
    FLAGS="--optimize"
    cat src/Env/Prod.elm > src/Env/Env.elm
    sed 's/Env.Prod/Env.Env/' src/Env/Env.elm > tmp_src_Env  # -i is incompatible on alpine
    cat tmp_src_Env > src/Env/Env.elm
    rm tmp_src_Env
elif [ "$2" = "stag" ]
then
    FLAGS="--optimize"
else
    FLAGS="--debug"
fi

# third param is version (tag or commit)
if [ "$3" ]
then
    VERSION=$(echo "$3" | tr / -)
else
    VERSION=dev
fi

# fourth param is timestamp
if [ "$4" ]
then
    VERSION_DATE=$(echo "$4" | cut -dT -f1,1)
else
    VERSION_DATE=$(date -I)
fi

echo "module Env.Util.BuildVariables exposing (..)" >  src/Env/Util/BuildVariables.elm
echo "version : String"                             >> src/Env/Util/BuildVariables.elm
echo "version = \"$VERSION\""                       >> src/Env/Util/BuildVariables.elm
echo "versionDate : String"                         >> src/Env/Util/BuildVariables.elm
echo "versionDate = \"$VERSION_DATE\""              >> src/Env/Util/BuildVariables.elm

#########
# BUILD #

FAILED="0"
for f in src/App/*.elm
do
    NAME=$(basename -s .elm "$f")
    NAME_LOWER=$(echo "$NAME" | tr '[:upper:]' '[:lower:]')
    echo ""
    echo "Building $NAME..."

    if [ $NAME_LOWER = 'skilltree' -o $NAME_LOWER = 'list' ]
    then
        echo "Generating $NAME_LOWER.html"
        sed "s/\\\$APP_NAME\\\$/$NAME/" app-index-template.html > "../public/$NAME_LOWER.html"
    fi

    if $ELM make $FLAGS "$f" --output "../public/bundle-$NAME.js"
    then
        echo "Build of $NAME: OK"
    else
        echo "Build of $NAME: ERROR"
        FAILED="1"
    fi
done


###########
# CLEANUP #

cat src/Env/tmp-backup-Env.elm > src/Env/Env.elm
rm src/Env/tmp-backup-Env.elm
cat src/Env/Util/tmp-backup-BuildVariables.elm > src/Env/Util/BuildVariables.elm
rm src/Env/Util/tmp-backup-BuildVariables.elm

if [ "$FAILED" = "1" ]
then
    exit 1
fi
